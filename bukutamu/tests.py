from django.test import TestCase, Client
from .models import BukuTamu

# Create your tests here.
class Test123(TestCase):
    def test_adakah_url_formulir(self):
        response = Client().get('/formulir/')
        self.assertEquals(response.status_code, 200)

    def test_adakah_url_formulir_ada_text_BUKU_PESERTA(self):
        response = Client().get('/formulir/')
        html_return = response.content.decode('utf8')
        self.assertIn("BUKU PESERTA", html_return)
        self.assertIn("Kirim", html_return)

    def test_adakah_formulir_templatenya(self):
        response = Client().get('/formulir/')
        self.assertTemplateUsed(response, 'formulir.html')

    def test_apakah_submit_dikirim_ke_hasil(self):
        response = Client().get('/hasil/')
        self.assertEquals(response.status_code, 200)

    def test_adakah_hasil_templatenya(self):
        response = Client().get('/hasil/')
        html_return = response.content.decode('utf8')
        self.assertTemplateUsed(response, 'hasil.html')
        self.assertIn("BUKU PESERTA", html_return)

    def test_apakah_sudah_ada_model_buku_peserta(self):
        BukuTamu.objects.create(nama="INI NAMA", kegiatan="INI KEGIATAN")
        hitung_jumlah_buku = BukuTamu.objects.all().count()
        self.assertEquals(hitung_jumlah_buku, 1)

    def test_adakah_handle_post_di_halaman_hasil(self):
        response = Client().post('/hasil/',{'nama':'INI NAMA','kegiatan':'INI KEGIATAN'})
        html_return = response.content.decode('utf8')
        self.assertIn("BUKU PESERTA", html_return)
        self.assertIn("INI NAMA", html_return)
        self.assertIn("INI KEGIATAN", html_return)
