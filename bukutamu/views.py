from django.shortcuts import render
from django.http import HttpResponse
from .models import BukuTamu

# Create your views here.
def get_formulir(request):
    response ={}
    return render(request,'formulir.html', response)

def get_hasil(request):
    if request.method == 'POST':
        BukuTamu.objects.create(nama=request.POST['nama'], kegiatan=request.POST['kegiatan'])
    isi_data_buku_tamu = BukuTamu.objects.all()
    response ={'data':isi_data_buku_tamu}
    return render(request,'hasil.html', response)


